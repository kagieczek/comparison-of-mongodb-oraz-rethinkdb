from pymongo import MongoClient
import time
import sys
from bson.objectid import ObjectId

class DbTest:
        @staticmethod
        def get_db():
                client = MongoClient(host="localhost", port=27017)
                db = client['test-db']
                return db

        @staticmethod
        def read_post(mydb):
              cursor = mydb.posts.find({}, {"autor_imie":1})


def get_time():
    return time.time() * 1000 * 1000

def main():
        test = DbTest()
        db = test.get_db()
        #Number of Docs
        documentsNumber = int(sys.argv[1])
        #Init star-time
        start = get_time()
        #Loop
        for i in range(0, documentsNumber):
                test.read_post(db)
        #Init end-time
        end = get_time()-start
        print("Sum:")
        print(str(end))

if __name__ == "__main__":
        main()