from pymongo import MongoClient
import time
import sys
from bson.objectid import ObjectId

class DbTest:
        @staticmethod
        def get_db():
                client = MongoClient(host="localhost", port=27017)
                db = client['test-db']
                return db

        @staticmethod
        def update_post(mydb, idc):
            post_updated = mydb.posts.update_one(
                {"_id": ObjectId(idc)},
                {
                    "$set": {
                            "autor_imie": "Konrad",
                            "autor_nazwisko": "Nowaczek",
                            "komentarz": "tekst"
                    }
                }
            )
        #for_check
        #print("Zmodyfikowany post: %s" % str(post_updated.matched_count))
        def get_name_post(mydb):
            cursor = mydb.posts.find({}, {"autor_imie": 1})
            return cursor


def get_time():
    return time.time() * 1000 * 1000

def main():
        test = DbTest()
        db = test.get_db()
        #Number of Docs
        documentsNumber = int(sys.argv[1])
        #Init star-time
        start = get_time()
        #Read
        data = test.get_name_post(db)
        #Take ID
	id_sel = raw_input ("Podaj ID posta")
	#Loop
        for i in range(0, documentsNumber):
                test.update_post(db, id_sel)
        #Init end-time
        end = get_time()-start
        print("Sum:")
        print(str(end))

if __name__ == "__main__":
        main()