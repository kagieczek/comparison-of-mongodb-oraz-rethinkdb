import sys
import time
from rethinkdb import RethinkDB

r = RethinkDB()

#Connect to subsequent commands
r.connect('localhost', 28015).repl()

#Drop DB
r.db_drop('test_db').run()

#Create DB
r.db_create('test_db').run()

#Create table
r.db('test_db').table_create('test').run()

#Def time
def get_time():
    return time.time()

#Init star-time
start = get_time()

#Get Number of Docs
documentsNumber = int(sys.argv[1])

#Loop
i = 0
while i < documentsNumber:
        r.db("test_db").table("test").insert([{
        "autor_imie" : "Jan",
        "autor_nazwisko" : "Kowalski",
        "autor_wiek" : 15,
        "komentarz": "text"
        }]).run()
        i += 1

#Init end-time
end = get_time()-start

print("Sum:")
print(str(end))